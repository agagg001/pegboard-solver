package com.agaggi;

import java.util.Arrays;
import java.util.Random;

public class Pegboard {

    private final int[][] board;

    /**
     * Class simulates a pegboard.
     * @param size The size of the pegboard (i.e. 4 -> 4x4)
     */
    public Pegboard(int size) {

        this.board = new int[size][size];
    }


    /**
     * Copy constructor used for deepcopies.
     * @param pegboard The pegboard object to be deepcopied
     */
    public Pegboard(Pegboard pegboard) {

        this.board = new int[pegboard.board.length][pegboard.board.length];

        for (int i = 0; i < this.board.length; i++) {

            System.arraycopy(pegboard.board[i], 0, this.board[i], 0, this.board.length);
        }
    }


    /**
     * Fills the pegboard with values of 1 except for a random location.
     * Spaces containing '1' represent spaces with pegs.
     */
    public void initialize() {

        for (int[] row : this.board) {

            Arrays.fill(row, 1);
        }

        Random rand = new Random();
        this.board[rand.nextInt(this.board.length)][rand.nextInt(this.board.length)] = 0;
    }


    /**
     * Prints the spaces of a pegboard in a nxn format.
     */
    public void print() {

        System.out.println();

        for (int[] row : this.board) {

            for (int peg : row) {

                System.out.print(peg + " ");
            }

            System.out.println();
        }
    }


    /**
     * Formats the pegs into a 1D string (i.e. 0111111111111111). Used for checking visited states.
     * @return A 1D string consisting of the values in a pegboard
     */
    @Override
    public String toString() {

        String string = "";

        for (int[] row : this.board) {

            for (int peg : row) {

                string += peg;
            }
        }

        return string;
    }


    /**
     * Getter method for the 2D board array
     * @return The board array containing the "pegs"
     */
    public int[][] getBoard() {

        return this.board;
    }


    /**
     * Sets the value at a given location on the board.
     *
     * @param x The x-coordinate
     * @param y The y-coordinate
     * @param value 0 or 1; whether a peg will be at that location
     */
    void setBoard(int x, int y, int value) {

        this.board[x][y] = value;
    }
}
